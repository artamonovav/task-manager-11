package ru.t1.artamonov.tm.api;

import ru.t1.artamonov.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
