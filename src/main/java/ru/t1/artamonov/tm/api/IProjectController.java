package ru.t1.artamonov.tm.api;

public interface IProjectController {

    void clearProjects();

    void createProject();

    void removeProjectById();

    void removeProjectByIndex();

    void showProjectById();

    void showProjectByIndex();

    void showProjects();

    void updateProjectById();

    void updateProjectByIndex();

}
