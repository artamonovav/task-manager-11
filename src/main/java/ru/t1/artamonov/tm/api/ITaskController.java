package ru.t1.artamonov.tm.api;

public interface ITaskController {

    void clearTasks();

    void createTask();

    void removeTaskById();

    void removeTaskByIndex();

    void showTaskById();

    void showTaskByIndex();

    void showTasks();

    void updateTaskById();

    void updateTaskByIndex();

}
